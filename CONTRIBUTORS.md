### Project head
Jakub M. Bartoszewicz \<jakub.bartoszewicz(at)hpi.de\>

### Active Contributors
Jakub M. Bartoszewicz \<jakub.bartoszewicz(at)hpi.de\>
  
### Former Contributors
Anja Seidel
Ulrich Genske
Ferdous Nasri
Melania Nowicka
